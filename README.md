# Test_WebServiceFramework



## Informations sur l'étudiant
**Nom** : ANDRIANIAINA Soavin'Ny Avo Sandratra
**ETU** : 1540 

## Information sur le projet
- Les codes sources se trouvent dans src/java/Controller
- Les dépendances du projet précedent se trouve dans le dossier `dep`
- Le project précedent exporté en jar porte le nom `Test_exportation_jar.jar`

## Etapes réalisées pour le test
- On utilise le fichier SendData.jsp dans web/SendData.jsp pour envoyer des informations vers le frontServlet avec l'url de la méthode de AndranaController
- La page Display.jsp permet de s'assurer que les paramètres ont été effectivement receptionner par le controller test