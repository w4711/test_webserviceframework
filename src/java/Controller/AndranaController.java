/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Controller;

import Model.ModelView;
import PersonnalAnnotation.UrlAnnotation;
import java.util.HashMap;

/**
 *
 * @author HP H
 */
public class AndranaController {
    int id;
    String nom;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }
    
    @UrlAnnotation(url = "retrieve12.do")
    public ModelView retrieve12(){
        ModelView resultAsModelView = new ModelView();
        resultAsModelView.setUrlReturn("Display.jsp");
        HashMap<String, Object> data = new HashMap<String, Object>();
        data.put("nombre", id);
        data.put("nom", nom);
        resultAsModelView.setData(data);
        return resultAsModelView;
    }

    @Override
    public String toString() {
        return "AndranaController{" + "id=" + id + ", nom=" + nom + '}';
    }
    
        
}
