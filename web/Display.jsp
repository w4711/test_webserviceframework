<%-- 
    Document   : Display
    Created on : 21 oct. 2022, 16:59:29
    Author     : HP H
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Displaying data</h1>
        <p><% out.print(request.getAttribute("nombre")); %></p>
        <p><% out.print(request.getAttribute("nom")); %></p>
    </body>
</html>
